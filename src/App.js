import logo from './logo.svg';
import './App.css';
import React, { useEffect, useState } from 'react';

function App() {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  const hash = [
    { number: 1, color: '#0289df'},
    { number: 2, color: '#e43458'},
    { number: 3, color: '#8b7b5b'},
    { number: 4, color: '#b85e61'},
    { number: 5, color: '#7f00ca'},
    { number: 7, color: '#017a67'},
    { number: 8, color: '#6953fe'},
    { number: 9, color: '#475c6c'},
    { number: 6, color: '#00b19f'},
  ]

  const shuffle = (array) => {
    let currentIndex = array.length;
    let randomIndex;
  
    // While there remain elements to shuffle...
    while (currentIndex != 0) {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;
  
      // And swap it with the current element.
      [array[currentIndex], array[randomIndex]] = [
        array[randomIndex],
        array[currentIndex],
      ];
    }
  
    return array;
  };

  const clickRand = () =>{
    setLoading(true);
    let random = hash;
    setData(shuffle(random));
    setLoading(false);
  }

  useEffect(() => {
    setData(hash);
  },[])

  return (
    <div className="App container">
      { loading === false ? (
          <>
            {
              data.map((d, index) => {
                let number = index+1;
                return (
                  <>
                    {
                      number === 1 ? (
                        <div key={number} onClick={clickRand} className='box box1'>
                          <div className='box-bg' style={{background: d.color}}>
                            <div className='number'>{d.number}</div>
                          </div>
                        </div>
                      ) : number === 2 ? (
                        <div key={number} onClick={clickRand} className='box box2'>
                          <div className='box-bg' style={{background: d.color}}>
                            <div className='number'>{d.number}</div>
                          </div>
                        </div>
                      ) : number === 3 ? (
                        <div key={number} onClick={clickRand} className='box box3'>
                          <div className='box-bg' style={{background: d.color}}>
                            <div className='number'>{d.number}</div>
                          </div>
                        </div>
                      ) : number === 4 ? (
                        <div key={number} onClick={clickRand} className='box box4'>
                          <div className='box-bg' style={{background: d.color}}>
                            <div className='number'>{d.number}</div>
                          </div>
                        </div>
                      ) : number === 5 ? (
                        <div key={number} onClick={clickRand} className='box box5'>
                          <div className='box-bg' style={{background: d.color}}>
                            <div className='number'>{d.number}</div>
                          </div>
                        </div>
                      ) : number === 7 ? (
                        <div key={number} onClick={clickRand} className='box box6'>
                          <div className='box-bg' style={{background: d.color}}>
                            <div className='number'>{d.number}</div>
                          </div>
                        </div>
                      ) : number === 6 ? (
                        <div key={number} onClick={clickRand} className='box box7'>
                          <div className='box-bg' style={{background: d.color}}>
                            <div className='number'>{d.number}</div>
                          </div>
                        </div>
                      ) : number === 8 ? (
                        <div key={number} onClick={clickRand} className='box box8'>
                          <div className='box-bg' style={{background: d.color}}>
                            <div className='number'>{d.number}</div>
                          </div>
                        </div>
                      ) : number === 9 ? (
                        <div key={number} onClick={clickRand} className='box box9'>
                          <div className='box-bg' style={{background: d.color}}>
                            <div className='number'>{d.number}</div>
                          </div>
                        </div>
                      ) : null
                    }
                  </>
                )
              })
            }
          </>
        ) : 'Loading' 
      }
    </div>
  );
}

export default App;
